FROM adoptopenjdk/openjdk8:x86_64-ubuntu-jdk8u292-b10

RUN cd "/tmp" && \
    apt-get update && \
    # -----------------------------
    echo "installing basic tools ..." && \
    apt-get install -y software-properties-common gnupg apt-transport-https ca-certificates && \
    apt-get -y install clang-9 && \
    apt-get -y install make && \
    # -----------------------------
    # -----------------------------
    echo 
